package com.sourcico.slicetalk.myapplication;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.sourcico.slicetalk.myapplication.activity.SecondActivity;
import com.sourcico.slicetalk.myapplication.databinding.ActivityMainBinding;
import com.sourcico.slicetalk.myapplication.util.LiveDataValueViewModel;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements LifecycleObserver {

    public final String TAG = "SourcicoExample";
    ActivityMainBinding mBinding;

    LiveDataValueViewModel liveDataValueViewModel;

    Integer randomNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.wtf(TAG, "---# onCreate #---");

        initComponents();
        setComponents();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void connectListener() {

        Toast.makeText(this, "Simulate Connect Listener", Toast.LENGTH_SHORT).show();
    }

    private void initComponents() {

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        liveDataValueViewModel = ViewModelProviders.of(this).get(LiveDataValueViewModel.class);

    }


    private void setComponents() {

        getLifecycle().addObserver(this);

        mBinding.buttonNavigateTo.setOnClickListener(v -> startActivity(new Intent(MainActivity.this, SecondActivity.class)));

        mBinding.buttonFixSolution.setOnClickListener(v ->
            {
                liveDataValueViewModel.getmRandomValue().setValue(randomNumber);
//                liveDataValueViewModel.mRandomValueLiveData.setValue(randomNumber);
            }
        );

        //make subscribe  - Comment this write with LiveData
        // Update the TextView when the ViewModel is changed.
        liveDataValueViewModel.getmRandomValue().observe(this, value -> {
            if (value != null) {
                randomNumber = value;
                setTestDummyData();
            }
        });


//        Observer<Integer> integerObserver = new Observer<Integer>() {
//            @Override
//            public void onChanged(@Nullable Integer integer) {
//                if (integer != null) {
//                    randomNumber = integer;
//                    setTestDummyData();
//                }
//            }
//        };
//
//        liveDataValueViewModel.mRandomValueLiveData.observe(this, integerObserver);

        mBinding.buttonSimulateData.setOnClickListener(v ->
        {
            randomNumber = new Random().nextInt(1000);
            setTestDummyData();
        });


    }

    private void setTestDummyData() {

        mBinding.textRandomNumber.setText("Random number:\n\n" + randomNumber);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "---# onStart #---");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "---# onResume #---");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "---# onPause #---");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "---# onStop #---");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "---# onDestroy #---");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.w(TAG, "---# onRestart #---");
    }

}













//        Observer<Integer> integerObserver = value -> {
//            if (value != null) {
//                randomNumber = value;
//                setTestDummyData();
//            }
//        };
//        liveDataValueViewModel.mRandomValueLiveData.observe(this, integerObserver);