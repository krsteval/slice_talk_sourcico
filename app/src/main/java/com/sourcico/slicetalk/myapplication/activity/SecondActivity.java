package com.sourcico.slicetalk.myapplication.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sourcico.slicetalk.myapplication.R;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
    }
}
